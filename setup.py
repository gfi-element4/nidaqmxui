#!/usr/bin/env python

from distutils.core import setup

setup(name='nidaqmxui',
      version = '0.0.1',
      author = 'Callum Doolin',
      author_email = 'callum@gfisystems.ca',
      packages = ["nidaqmxui"],


      install_requires = [
            'numpy',
            'matplotlib',
            'ipympl',
            'lazyclass',
            'nidaqmx',
      ]

)
