

import nidaqmx
import nidaqmx.stream_readers
import asyncio

from lazyclass import LazyClass, dotdict
import threading
import ipywidgets as widgets

from nidaqmx.constants import TerminalConfiguration

import numpy as np
import matplotlib.pyplot as plt




class NidaqReader(LazyClass):
    """NidaqReader encapsulates the nidaqmx Task.

    It starts a background thread to perform non-blocking data collection.
    """
    def __init__(self, rate=1000, framesize=None):
        """
        
        Args:
            rate (float): Samplerate for data collection (samples / s)
            framesize (int): Number of samples to collect at a time
        """
        self.task = nidaqmx.Task()
        self._rate = rate
        
        
        self._thread = None
        self._to_take = 0
        self._go = False
        
        self._data = [[]]
        self._callbacks = []
        self._loop = asyncio.get_event_loop()
        
        self.framesize = int(rate * .2) if framesize is None else framesize
        
    @property
    def rate(self):
        return self._rate

    @rate.setter
    def rate(self, rate):
        self._rate = rate
#         self.task.timing.cfg_samp_clk_timing(
#             rate=rate, 
#             sample_mode=nidaqmx.constants.AcquisitionType.CONTINUOUS, 
#             samps_per_chan=1000)

    @property
    def ai_channels(self):
        return self.task._ai_channels
    
    @property
    def nchannels(self):
        return len(self.ai_channels)
    
    def start_task(self):
        self.task.timing.cfg_samp_clk_timing(
            rate=self._rate, 
            sample_mode=nidaqmx.constants.AcquisitionType.CONTINUOUS, 
            samps_per_chan=1000)
        
        self.reader = nidaqmx.stream_readers.AnalogMultiChannelReader(self.task.in_stream)
        self.data = [[] for i in range(self.nchannels)]
        
        self.task.start()
        
    def stop_task(self):
        self.task.stop()
        
    @property
    def n(self):
        return len(self.data[0])
    
    @property
    def running(self):
        return self._go
        

@NidaqReader.method()
def _main(self):
    self.start_task()
    
    buff = np.empty((self.nchannels, self.framesize))
    
    while self._go and self._to_take != 0:
        self.reader.read_many_sample(buff, number_of_samples_per_channel=self.framesize)
#         data = self.task.read(number_of_samples_per_channel=self.framesize)
    
        for i in range(self.nchannels):
            self.data[i] += buff[i].tolist()
            
        self._to_take -= self.framesize
        
    
    self._go = False
    self.stop_task()
    
    
@NidaqReader.method()
def start(self, nsamples=-1):
    if self._thread is None or not self._thread.is_alive():
        if nsamples > 0:
            self._to_take = self.framesize * (nsamples // self.framesize)
        else:
            self._to_take = -1
            
        self._go = True
        
        
        self._thread = threading.Thread(target=self._main)
        self._thread.start()
    else:
        print("thread already running")
        
@NidaqReader.method()
def stop(self, join=True):
    if self._thread is None or not self._thread.is_alive():
        print("thread not running")
    else:
        self._go = False
        if join:
            self._thread.join()
        
    



class Plotter(LazyClass):
    def __init__(self, reader, t=10):
        self.lines = []
        self.reader = reader
        
        self.init_fig()
        self.setup_ui()

        self.t = t
        
    def init_fig(self):
        plt.ioff()
        self.fig, self.ax = plt.subplots(1, 1, figsize=(8, 4))
        
        plt.ylim(-10, 10)
        plt.xlabel("Time (s)")
        plt.ylabel("Voltage (V)")
        plt.ion()


def set_t(self, t):
    self.n = int(t * self.reader.rate)
    self._t = np.arange(-self.n + 1, 1) / self.reader.rate
    self.ax.set_xlim(self._t[0], self._t[-1])

    if 'time' in self.ui:
        self.ui.time.value = f"{self.t}"

@Plotter.property(fset=set_t)
def t(self):
    return self.n / self.reader.rate

@Plotter.method()
def setup(self):
    for l in self.lines:
        l.remove()
    
    self.lines = []
    for i, ch in enumerate(self.reader.ai_channels.channel_names):
        l, = plt.plot([], [], '-', c=f"C{i}", label=ch)
        self.lines.append(l)
        
    self.ax.legend(loc='upper left', fontsize='small')

@Plotter.method()
def update(self):
    n = self.reader.n
    if n > self.n:
        n = self.n
    
    for i in range(self.reader.nchannels):
        self.lines[i].set_data(self._t[-n:], self.reader.data[i][-n:])
        
    self.fig.canvas.draw()
    self.fig.canvas.flush_events()
    
    if self.ui.refresh.value and self.reader.running:
        asyncio.get_event_loop().call_later(0.2, self.update)

@Plotter.method()
def on_play(self, e):
    self.setup()
    self.reader.start()
    self.ui.play.disabled = True
    self.ui.stop.disabled = False
    asyncio.get_event_loop().call_later(1, self.update)
    
@Plotter.method()
def on_stop(self, e):
    self.reader.stop(join=True)
    self.ui.play.disabled = False
    self.ui.stop.disabled = True

@Plotter.method()
def on_refresh(self, e):
    if e['name'] == '_property_lock' and 'value' in e['new'] and e['new']['value']:
        asyncio.get_event_loop().call_soon(self.update)

@Plotter.method()
def on_submit_time(self, w):
    self.t = float(w.value)

@Plotter.method()
def setup_ui(self):
    self.ui = dotdict(
        play = widgets.Button(description="", icon='play'),
        stop = widgets.Button(description="", icon='stop'),
        refresh = widgets.ToggleButton(description="", icon='refresh', value=True),
        time = widgets.Text(value="", description="Time (s):"),
    )
    
    self.ui.play.on_click(self.on_play)
    self.ui.stop.on_click(self.on_stop)
    self.ui.refresh.observe(self.on_refresh)
    self.ui.time.on_submit(self.on_submit_time)
    
@Plotter.method()
def display(self):
    return widgets.VBox([
        self.fig.canvas,
        widgets.HBox(list(self.ui.values())),
    ])
