
nidaqmxui
=========

`nidaqmxui` is a module for live plotting data collected with [nidaqmx](https://nidaqmx-python.readthedocs.io/en/latest/) in a Jupyter notebook.

